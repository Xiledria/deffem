if not DeffEM then DeffEM = {} end

function DeffEM.OnEvent()
    if arg1 == "DeffEM" then
        SLASH_DeffEM1 = "/DeffEM"
        SlashCmdList["DeffEM"] = DeffEM.ChatCommandHandler
        DeffEM.Initialize()
    end
end

function DeffEM.GetSub(msg)
    if msg then
        local a,b,c=strfind(msg, "(%S+)")
        if a then
            return c, strsub(msg, b+2)
        else
            return ""
        end
    end
end

function DeffEM.ChatCommandHandler(msg)
    if not msg or msg =="" then
        return
    end
end

function DeffEM.RcvMsg(self, event, prefix, message, channel, sender)
    if prefix ~= "DeffEM" then
        return
    end
    local name,_ = UnitName("player")
    if name == sender then
        return
    end
    local submsg
    message,submsg = DeffEM.GetSub(message)
    if message == "event" then
        local eventId, submsg = DeffEM.GetSub(submsg)
        local plvl, version = DeffEM.GetSub(submsg)
        eventId = tonumber(eventId) plvl = tonumber(plvl) version = tonumber(version)
        local event = DeffEM.GetEvent(eventId)
        if event.version ~= version then
--            if plvl > event.plvl or (plvl == event.plvl and event.version < version) then
            if event.version < version then
                DeffEM.RequestEvent(eventId, sender)
            end
        end
    elseif message == "request" then
        DeffEM.SendEvent(tonumber(submsg), sender)
    elseif message == "ei" then
        local id, submsg = DeffEM.GetSub(submsg)
        id = tonumber(id)
        if     id == 1 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            local plvl, submsg = DeffEM.GetSub(submsg)
            local version, submsg = DeffEM.GetSub(submsg)
            local teleport, name = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId) plvl = tonumber(plvl) version = tonumber(version)
            local event = DeffEM.GetTempEvent(eventId, sender)
            event.plvl = plvl
            event.version = version
            event.teleport = teleport
            event.name = name
        elseif id == 2 then
            local cmd, submsg = DeffEM.GetSub(submsg)
            local eventId, submsg = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId)
            -- @TODO: make some difference between app and end if required, otherwise delete different packets, lol
            local event = DeffEM.GetTempEvent(eventId, sender)
            event.description = event.description..submsg
        elseif id == 3 then
            local cmd, submsg = DeffEM.GetSub(submsg)
            local eventId, submsg = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId)
            -- @TODO: make some difference between app and end if required, otherwise delete different packets, lol
            local event = DeffEM.GetTempEvent(eventId, sender)
            event.rules = event.rules..submsg
        elseif id == 4 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId)
            local event = DeffEM.GetTempEvent(eventId, sender)
            local value
            repeat
                value, submsg = DeffEM.GetSub(submsg)
                if value == "" then break end
                table.insert(event.rewards, value)
            until submsg == nil
        elseif id == 5 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            local index, description = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId) index = tonumber(index)
            local event = DeffEM.GetTempEvent(eventId, sender)
            event.macros[index] = { }
            event.macros[index].description = description
        elseif id == 6 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            local index, submsg = DeffEM.GetSub(submsg)
            local commandIndex, command = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId) index = tonumber(index) commandIndex = tonumber(commandIndex)
            local event = DeffEM.GetTempEvent(eventId, sender)
            if not event.macros[index].commands then event.macros[index].commands = { } end
            event.macros[index].commands[commandIndex] = command
        elseif id == 7 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId)
            local event = DeffEM.GetTempEvent(eventId, sender)
            local name = ""
            repeat
                name, submsg = DeffEM.GetSub(submsg)
                if name == "" then break end
                table.insert(event.owners, name)
            until submsg == nil
        elseif id == 8 then
            local eventId, submsg = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId)
            local event = DeffEM.GetTempEvent(eventId, sender)
            DeffEM.UpdateEvent(eventId, sender)
        elseif id == 9 then
            local msgType, submsg = DeffEM.GetSub(submsg)
            local eventId, submsg = DeffEM.GetSub(submsg)
            local index, script = DeffEM.GetSub(submsg)
            eventId = tonumber(eventId) index = tonumber(index)
            local event = DeffEM.GetTempEvent(eventId, sender)
            if not event.macros[index].script then event.macros[index].script = "" end
            event.macros[index].script = event.macros[index].script..script
        end
    elseif message == "sync" then
        local hash, version = DeffEM.GetSub(submsg)
        if not version or version == "" then return end
        if tonumber(version) > tonumber(DeffEM.version) then
            DeffEM.print("Your DeffEM version is out of date!")
            DeffEM.print("Download last version at")
            DeffEM.print("https://bitbucket.org/Xiledra/DeffEM")
        end
        -- do not sync on different addon versions
        if version ~= DeffEM.version then return end
        if hash ~= DeffEMSV.EventHash then
            DeffEM.SendAddonMessage("desync", sender)
            DeffEM.SendEventBasics(sender)
        end
    elseif message == "desync" then
        DeffEM.SendEventBasics(sender)
    end
end

function DeffEM.GetSortedTable(t)
    orderedPairs(t)
    local tmp = t["__orderedIndex"]
    t["__orderedIndex"] = nil
    return tmp
end

function DeffEM.SendAddonMessage(message, target)
    if not target then
        SendAddonMessage("DeffEM", message, "GUILD")
    else
        SendAddonMessage("DeffEM",message, "WHISPER", target)
    end
end

