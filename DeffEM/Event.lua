if not DeffEM then DeffEM = {} end

function DeffEM.NewEvent()
    local event = { }
    event.name = ""
    event.description = ""
    event.rules = ""
    event.teleport = "gmi"
    event.plvl = 0
    event.version = 0
    event.rewards = { }
    event.macros = { }
    event.owners = { }
    return event
end

function DeffEM.GetEventsCount()
    local counter = 0
    for _,_ in pairs(DeffEMSV.events) do
        counter = counter + 1
    end
    return counter
end

function DeffEM.SyncEvents()
    DeffEM.SendAddonMessage("sync "..DeffEMSV.EventHash.. " "..DeffEM.version)
end

function DeffEM.CalculateEventHash()
    local string = ""
    for i, event in orderedPairs(DeffEMSV.events) do
        if tonumber(i) then
            string=string..i..event.plvl..event.version
        end
    end
    DeffEMSV.EventHash = sha1.sha1(string)
    DeffEMSV.events["__orderedIndex"] = nil
end

function DeffEM.PrintEventInfo()
    for i, event in orderedPairs(DeffEMSV.events) do
        local string = "EVENT: "..event.name.."(ID: "..i..")\n"..event.description.."\n"..event.rules.."\n".."Owners: "
        local first = true
        for index, owner in pairs(event.owners) do
            if not first then
                string=string..", "..owner
            else
                first = nil
                string=string..owner
            end
        end
        string=string.."\nRewards:\n"
        for index, reward in pairs(event.rewards) do
            string=string.."   "..index..". místo: "..reward.." EM\n"
        end
        string=string.."Teleport:\n   "..event.teleport
        DeffEM.print(string)
    end
    DeffEMSV.events["__orderedIndex"] = nil
end

function DeffEM.UpdateEvent(eventId, sender)
    local new
    if not DeffEMSV.events[eventId] then
        DeffEMSV.events[eventId] = DeffEM.NewEvent()
        new = true
    end
    if not new then
        --@TODO: check plvl
        if DeffEMSV.events[eventId].version >= DeffEM.GetTempEvent(eventId, sender).version then
            return
        end
    end
    local event = DeffEMSV.events[eventId]
    local tempEvent = DeffEM.GetTempEvent(eventId, sender)
    event.name = tempEvent.name
    event.description = tempEvent.description
    event.rules = tempEvent.rules
    event.teleport = tempEvent.teleport
    event.plvl = tempEvent.plvl
    event.version = tempEvent.version
    event.rewards = tempEvent.rewards
    event.macros = tempEvent.macros
    event.owners = tempEvent.owners
    DeffEM.temp[sender][eventId] = nil
    DeffEM.CalculateEventHash()
    if new then 
        DeffEMSV.EventPage = 1
        DeffEM.ShowEventList(0)
        DeffEM.print("Received new event "..eventId.." from "..sender.."!")
    else
        DeffEM.print("Received updated version of event "..eventId.. " from "..sender.."!")
    end
end

function DeffEM.GetEvent(eventId)
    if not DeffEMSV.events[eventId] then return DeffEM.NewEvent() end
    return DeffEMSV.events[eventId]
end

function DeffEM.SendEventBasics(player)
    for id, event in pairs(DeffEMSV.events) do
        DeffEM.SendAddonMessage("event "..id.." "..event.plvl.." "..event.version, player)
    end
end

function DeffEM.GetTempEvent(eventId, sender)
    if not DeffEM.temp[sender] then DeffEM.temp[sender] = { } end
    local events = DeffEM.temp[sender]
    if not events[eventId] then events[eventId] = DeffEM.NewEvent() end
    return events[eventId]
end

function DeffEM.StartRecruit(count)
    Invajter.Toggle(0)
    Invajter.ChangeMaximum(count)
    Invajter.ToggleIncludeSelf(1)
    SendChatMessage(".whisper on")
end

-- @TODO - not real todo, just to find it later - latest ei = 9
function DeffEM.SendEvent(eventId, player)
    local event = DeffEMSV.events[eventId]
    DeffEM.SendAddonMessage("ei 1 "..eventId.." "..event.plvl.." "..event.version.." "..event.teleport.." "..event.name, player)
    local maxMsgTxtLen = 200
    local max = ceil(event.description:len() / maxMsgTxtLen)
    for i=1,max do
        if i == max then 
            DeffEM.SendAddonMessage("ei 2 end "..eventId.." "..string.sub(event.description, (i-1)*maxMsgTxtLen + 1, i*maxMsgTxtLen), player)
        else
            DeffEM.SendAddonMessage("ei 2 app "..eventId.." "..string.sub(event.description, (i-1)*maxMsgTxtLen + 1, i*maxMsgTxtLen), player)
        end
    end
    for i=1,max do
        if i == max then 
            DeffEM.SendAddonMessage("ei 3 end "..eventId.." "..string.sub(event.rules, (i-1)*maxMsgTxtLen + 1, i*maxMsgTxtLen), player)
        else
            DeffEM.SendAddonMessage("ei 3 app "..eventId.." "..string.sub(event.rules, (i-1)*maxMsgTxtLen + 1, i*maxMsgTxtLen), player)
        end
    end
    local msg = "ei 4 "..eventId.." "
    for i, rew in pairs(event.rewards) do
        msg=msg.." "..rew
    end
    DeffEM.SendAddonMessage(msg, player)
    for i, macro in pairs(event.macros) do
        DeffEM.SendAddonMessage("ei 5 "..eventId.." "..i.." "..macro.description, player)
        if macro.commands then
            for j, command in pairs(macro.commands) do
                DeffEM.SendAddonMessage("ei 6 "..eventId.." "..i.." "..j.." "..command, player)
            end
        end
        if macro.script then
            local max = ceil(macro.script:len() / maxMsgTxtLen)
            for j=1,max do
                if j == max then
                    DeffEM.SendAddonMessage("ei 9 end "..eventId.." "..i.." "..string.sub(macro.script, (j-1)*maxMsgTxtLen + 1, j*maxMsgTxtLen), player)
                else
                    DeffEM.SendAddonMessage("ei 9 app "..eventId.." "..i.." "..string.sub(macro.script, (j-1)*maxMsgTxtLen + 1, j*maxMsgTxtLen), player)
                end
            end
        end
    end
    local owners = ""
    for i, owner in pairs(event.owners) do
        owners = owners.." "..owner
    end
    DeffEM.SendAddonMessage("ei 7 "..eventId..owners, player)
    DeffEM.SendAddonMessage("ei 8 "..eventId, player)
end

function DeffEM.RequestEvent(eventId, player)
    DeffEM.SendAddonMessage("request "..eventId, player)
end
