if not DeffEM then DeffEM = {} end
if not DeffEM.UI then DeffEM.UI = {} end
-- path to graphical resources
local path = "Interface\\AddOns\\DeffEM\\graphics\\"

function DeffEM.DragIcon()
    local xpos,ypos = GetCursorPosition()
    local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()
    xpos = xmin-xpos/UIParent:GetScale()+70
    ypos = ypos/UIParent:GetScale()-ymin-70
    DeffEMSV["minimap"] = math.deg(math.atan2(ypos,xpos))
    DeffEMMinimap:SetPoint("TOPLEFT",52-(80*cos(DeffEMSV["minimap"])),(80*sin(DeffEMSV["minimap"]))-52)
end

function DeffEM.HandleMouseClick(_, ButtonId)
    if (ButtonId == "RightButton") then
        DeffEM.ResetFramePosition()
    else
        DeffEM.ToggleFrameVisibility()
    end
end

function DeffEM.print(text)
    print("|cffdfff00"..text)
end

function DeffEM.ToggleFrameVisibility()
    local f = DeffEM.UI.MainFrame
    if f:IsVisible() then
        f:Hide()
    else
        f:Show()
    end
end

function DeffEM.MoveFrame()
    DeffEM.UI.MainFrame:StartMoving()
end

function DeffEM.StopMoveFrame()
    if not DeffEMSV["MainFrame"] then DeffEMSV["MainFrame"] = { } end
    local cfg = DeffEMSV["MainFrame"]
    local mf = DeffEM.UI.MainFrame
    mf:StopMovingOrSizing()
    local x,y,w,h = mf:GetRect() 
    cfg.x = x
    cfg.y = y
    cfg.w = w
    cfg.h = h
end

function DeffEM.InitButton(f, w, h, texture, s)
    if not s then
        f:SetFrameStrata("LOW")
    else
        f:SetFrameStrata(s)
    end
    f:SetWidth(w)
    f:SetHeight(h)
    local t = f:CreateTexture(nil,"BACKGROUND")
    t:SetTexture(texture)
    t:SetAllPoints(f)
    f.texture = t
    f:SetAlpha(.6)
end

function DeffEM.CreateTextFrame(width, height, texture, fontsize, alignPoint, parent, offsetx, offsety, frametype)
    if not parent then parent = DeffEM.UI.MainFrame end
    if not offsetx then offsetx = 0 end
    if not offsety then offsety = 0 end
    if not frametype then frametype = "Frame" end
    local frame = CreateFrame(frametype,nil,DeffEM.UI.MainFrame,UIPanelButtonTemplate)
    DeffEM.InitButton(frame, width, height, path..texture, "MEDIUM")
    frame:SetPoint(alignPoint, parent, offsetx, offsety)
    local textframe = frame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    textframe:SetTextColor(255,255,0,1)
    textframe:SetShadowColor(0,0,0,0.5)
    textframe:SetShadowOffset(2,-2)
    textframe:SetFont(path.."Play-Bold.TTF", fontsize, "OUTLINE")
    textframe:SetSpacing(6)
    textframe:SetSize(width - 10, height - 5)
    textframe:SetPoint("CENTER")
    textframe:SetWordWrap(1)
    return frame, textframe
end

function DeffEM.ShowEventInfo(eventId)
    local e = DeffEMSV.events[eventId]
    if not e then return end
    local macroCounter = 1
    local infoType = DeffEM.LastActiveInfoButton:GetID()
    if infoType == 1 then -- description
        DeffEM.UI.EventDescription:SetText(e.description)
    elseif infoType == 2 then -- rules
        DeffEM.UI.EventDescription:SetText(e.rules)
    elseif infoType == 3 then -- macros
        for i, macro in pairs(e.macros) do
             
            DeffEM.UI.Macros[i].text:SetText(macro.description)
            DeffEM.UI.Macros[i].frame:Show()
            DeffEM.UI.Macros[i].frame:Enable()
            DeffEM.UI.Macros[i].frame:SetID(i)
            macroCounter = macroCounter + 1
            if macroCounter > #DeffEM.UI.Macros then break end
        end
        DeffEM.UI.EventDescription:SetText("")
    end

    if macroCounter < #DeffEM.UI.Macros then
        for i=macroCounter, #DeffEM.UI.Macros do
            DeffEM.UI.Macros[i].frame:Hide()
        DeffEM.UI.Macros[i].frame:Disable()
        end
    end

    local string = ""
    for i, owner in pairs(e.owners) do
        string = string..owner..", "
    end
    DeffEM.UI.EventOwners:SetText(string:sub(1, -3))
    DeffEM.UI.EventTitle:SetText(e.name)
    DeffEMSV.LastShownEvent = eventId
    string = ""
    local orderString = ""
    for i, reward in pairs(e.rewards) do
        orderString = orderString..i..":\n"
        string = string..reward.."EM\n"
    end
    DeffEM.UI.EventRewardsList:SetText(string)
    DeffEM.UI.EventRewardsOrderList:SetText(orderString)
    DeffEM.UI.EventTeleport:SetText(e.teleport)
    return true
end

function DeffEM.ClickTeleportButton()
    SendChatMessage(".tele "..DeffEM.UI.EventTeleport:GetText())
end

function DeffEM.ClickStartButton()
    SendChatMessage(".event start "..DeffEMSV.LastShownEvent)
end

function DeffEM.ClickStopButton()
    SendChatMessage(".event stop "..DeffEMSV.LastShownEvent)
end

function DeffEM.ResetFramePosition()
    local _,_,w,h = UIParent:GetRect()
    DeffEMSV.MainFrame = { }
    if not DeffEMSV.MainFrame.scale then DeffEMSV.MainFrame.scale = 1 end
    DeffEMSV.MainFrame.x = (w - 800 * DeffEMSV.MainFrame.scale) / 2
    DeffEMSV.MainFrame.y = (h - 500 * DeffEMSV.MainFrame.scale) / 2
    local mf = DeffEM.UI.MainFrame
    if mf then -- if frame is initialized
        DeffEMSV.MainFrame.scale = 1
        mf:SetSize(800, 500)
        mf:ClearAllPoints()
        mf:SetPoint("CENTER")
    end
    ReloadUI()
end

function DeffEM.ShowEventList(startIndex)
    local i = 8
    for id, event in orderedPairs(DeffEMSV.events) do
        if tonumber(id) and id >= startIndex then
            if i == 0 then break end
                local item = DeffEM.UI.EventList[i]
                if id == DeffEMSV.LastShownEvent then
                    DeffEM.SetActiveEventButton(item.frame)
                end
                item.id:SetText(id)
                item.name:SetText(event.name)
                local ownerString = ""
                for j, owner in pairs(event.owners) do
                    ownerString = ownerString..owner..", "
                end
                item.owner:SetText(ownerString:sub(1, -3))
            i=i-1
        end
    end
    DeffEMSV.events["__orderedIndex"] = nil
    for j=1,i do
        local item = DeffEM.UI.EventList[j]
        item.id:SetText("")
        item.name:SetText("")
        item.owner:SetText("")
    end
end

function DeffEM.MoveToNextPage()
    if DeffEM.UI.EventList[1].id == "" then return end
    if DeffEM.GetEventsCount() <= 8 * DeffEMSV.EventPage then return end
    if DeffEM.LastActiveEventInfo then
        DeffEM.LastActiveEventInfo.activeTexture:Hide()
    end
    DeffEM.ShowEventList(tonumber(DeffEM.UI.EventList[1].id:GetText()) + 1)
    DeffEMSV.EventPage = DeffEMSV.EventPage + 1
end

function DeffEM.MoveToPreviousPage()
    if DeffEMSV.EventPage == 1 then return end
    if DeffEM.LastActiveEventInfo then
        DeffEM.LastActiveEventInfo.activeTexture:Hide()
    end
    DeffEMSV.EventPage = DeffEMSV.EventPage - 1
    local counter = 0
    for id, event in orderedPairs(DeffEMSV.events) do
        if counter >= (DeffEMSV.EventPage - 1) * 8 then
            DeffEMSV.events["__orderedIndex"] = nil
            DeffEM.ShowEventList(id)
            break
        end
        counter = counter + 1
    end
end

function DeffEM.SetActiveEventButton(self)
    if DeffEM.LastActiveEventInfo then
        DeffEM.LastActiveEventInfo.activeTexture:Hide()
    end
    DeffEM.LastActiveEventInfo = self
    self.activeTexture:Show()
end

function DeffEM.HandleEventInfoClick(self)
    local str = DeffEM.UI.EventList[self:GetID()].id:GetText()
    if str == "" then return end
    if DeffEM.ShowEventInfo(tonumber(str)) then
        DeffEM.SetActiveEventButton(self)
    end
end

function DeffEM.HandleMacroClick(self)
    local id = self:GetID()
    -- @TODO: make an function in Event.lua to retrieve current event info
    local event = DeffEMSV.events[DeffEMSV.LastShownEvent]
    local macros = event.macros[id]
    if macros.commands then 
        for i, command in pairs(macros.commands) do
            SendChatMessage(command)
        end
    end
    if macros.script then
        loadstring(macros.script)()
    end
end

function DeffEM.CreateActiveTexture(frame)
    t = frame:CreateTexture()
    t:SetTexture(path.."active")
    t:SetAllPoints(frame)
    frame.activeTexture = t
    t:Hide()
end

function DeffEM.ToggleEventSubInfo(self)
    DeffEM.LastActiveInfoButton.activeTexture:Hide()
    self.activeTexture:Show()
    DeffEM.LastActiveInfoButton = self
    DeffEM.ShowEventInfo(DeffEMSV.LastShownEvent)
end

function DeffEM.ReduceSize()
    if DeffEMSV.MainFrame.scale < 0.3 then return end
    DeffEMSV.MainFrame.scale = DeffEMSV.MainFrame.scale - 0.05
    DeffEM.UI.ScaleFrame:SetText(string.format("%.2f", DeffEMSV.MainFrame.scale))
end

function DeffEM.IncreaseSize()
    if DeffEMSV.MainFrame.scale >= 2.0 then return end
    DeffEMSV.MainFrame.scale = DeffEMSV.MainFrame.scale + 0.05
    DeffEM.UI.ScaleFrame:SetText(string.format("%.2f", DeffEMSV.MainFrame.scale))
end

function DeffEM.InitializeUI()
    if not DeffEMSV.EventPage then DeffEMSV.EventPage = 1 end
    if not DeffEMSV["minimap"] then DeffEMSV["minimap"] = 270 end
    if not DeffEMSV.MainFrame then DeffEM.ResetFramePosition() end
    if not DeffEMSV.MainFrame.scale then
        DeffEMSV.MainFrame.scale = 1
        DeffEMSV.MainFrame.w = nil
        DeffEMSV.MainFrame.h = nil
    end
    DeffEMSV.events["__orderedIndex"] = nil
    local width, height = 800 * DeffEMSV.MainFrame.scale, 500 * DeffEMSV.MainFrame.scale
    DeffEMMinimap:SetPoint("TOPLEFT",52-(80*cos(DeffEMSV["minimap"])),(80*sin(DeffEMSV["minimap"]))-52)
    DeffEM.UI.MainFrame = CreateFrame("Frame", "DeffEMFrame", UIParent)
    local mf = DeffEM.UI.MainFrame
    mf:SetMovable(true)
    mf:Hide()
    mf:SetFrameStrata("BACKGROUND")
    mf:SetWidth(width)
    mf:SetHeight(height)
    local t = mf:CreateTexture(nil,"BACKGROUND")
    t:SetTexture(path.."background")
    t:SetAllPoints(mf)
    mf.texture = t
    mf:SetPoint("BOTTOMLEFT",DeffEMSV["MainFrame"].x, DeffEMSV["MainFrame"].y)

    local descriptionFrame, ownersFrame, titleFrame, headerFrame, rewardsFrame, rewardsOrder, scaleFrame, f, w, h, fontsize
    h = height / 25
    fontsize = h / 10 * 7
    headerFrame, DeffEM.UI.Header = DeffEM.CreateTextFrame(width, h, "free", fontsize, "TOP")
    headerFrame:EnableMouse(true)
    headerFrame:RegisterForDrag("LeftButton")
    headerFrame:SetScript("OnDragStart", DeffEM.MoveFrame)
    headerFrame:SetScript("OnDragStop", DeffEM.StopMoveFrame)
    DeffEM.UI.Header:SetText("DeffEM By Xiledria v"..DeffEM.version)
    w = width / 2
    fontsize = h / 2
    h = h * 14
    descriptionFrame, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMRIGHT")
    t:SetJustifyH("LEFT")
    t:SetJustifyV("TOP")
    t:SetPoint("CENTER", 5, -10)
    t:SetText(event.description)
    DeffEM.UI.EventDescription = t
    w = width / 4
    h = h / 7
    fontsize = h / 3
    DeffEM.UI.Macros = { }
    for line=0,6 do
        for column=0,1 do
            local index = line * 2 + column + 1
            DeffEM.UI.Macros[index] = { }
            f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, column * w, -line * h, "Button")
            f:SetPushedTexture(path.."active") -- make button red on clicking
            f:Hide()
            f:EnableMouse(true)
            f:RegisterForClicks("LeftButtonUp")
            f:SetScript("OnClick", DeffEM.HandleMacroClick)
            DeffEM.UI.Macros[index].frame = f
            DeffEM.UI.Macros[index].text = t
        end
    end

    h = h / 2
    fontsize = h / 2
    teleFrame, DeffEM.UI.EventTeleport = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, 0, 2 * h, "Button")
    teleFrame:EnableMouse(true)
    teleFrame:RegisterForClicks("LeftButtonUp")
    teleFrame:SetScript("OnClick", DeffEM.ClickTeleportButton)
    f, t = DeffEM.CreateTextFrame(w, h, "", fontsize, "TOP", teleFrame, 0, teleFrame:GetHeight())
    t:SetText("Teleport:")

    w = w / 2
    f, DeffEM.UI.EventStart = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMLEFT", teleFrame, 0, -h, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.ClickStartButton)
    DeffEM.UI.EventStart:SetText("start event")

    f, DeffEM.UI.EventStop = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMRIGHT", teleFrame, 0, -h, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.ClickStopButton)
    DeffEM.UI.EventStop:SetText("stop event")

    scaleFrame, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMLEFT", headerFrame, w / 2, -h)
    t:SetText(string.format("%.2f", DeffEMSV.MainFrame.scale))
    DeffEM.UI.ScaleFrame = t

    w = w / 2
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "RIGHT", scaleFrame, w, 0, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.IncreaseSize)
    t:SetText("+")
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "LEFT", scaleFrame, -w, 0, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.ReduceSize)
    t:SetText("-")

    w = w * 4

    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMLEFT", f, 0, -h, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", ReloadUI)
    t:SetText("RELOAD UI")

    titleFrame, DeffEM.UI.EventTitle = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMRIGHT", headerFrame, 0, -h)
    f, t = DeffEM.CreateTextFrame(w, h, "", fontsize, "LEFT", titleFrame, -titleFrame:GetWidth())
    t:SetJustifyH("RIGHT")
    t:SetText("Event Name:")

    ownersFrame, DeffEM.UI.EventOwners = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMRIGHT", titleFrame, 0, -h)
    f, t = DeffEM.CreateTextFrame(w, h, "", fontsize, "LEFT", ownersFrame, -ownersFrame:GetWidth())
    t:SetJustifyH("RIGHT")
    t:SetText("Created By:")

    h = h * 8
    w = w / 2
    rewardsFrame, DeffEM.UI.EventRewardsList = DeffEM.CreateTextFrame(w, h, "free", fontsize, "BOTTOMRIGHT", ownersFrame, 0, -h)
    rewardsOrder, DeffEM.UI.EventRewardsOrderList = DeffEM.CreateTextFrame(w, h, "free", fontsize, "LEFT", rewardsFrame, -w)
    h = h / 8
    w = w * 2
    f, t = DeffEM.CreateTextFrame(w, h, "", fontsize, "TOPLEFT", rewardsOrder, -w)
    t:SetJustifyH("RIGHT")
    t:SetText("Rewards:")
    w = width / 4
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, -w, -1 * h, "Button")
    t:SetText("Description")
    DeffEM.LastActiveInfoButton = f
    DeffEM.CreateActiveTexture(f)
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetID(1)
    f:SetScript("OnClick", DeffEM.ToggleEventSubInfo)
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, -w, -2 * h, "Button")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetID(2)
    f:SetScript("OnClick", DeffEM.ToggleEventSubInfo)
    t:SetText("Rules")
    DeffEM.CreateActiveTexture(f)
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, -w, -3 * h, "Button")
    t:SetText("Macros")
    DeffEM.CreateActiveTexture(f)
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetID(3)
    f:SetScript("OnClick", DeffEM.ToggleEventSubInfo)

    w = w / 2
    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, -w, -4.9 * h, "Button")
    t:SetText(">>")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.MoveToNextPage)

    f, t = DeffEM.CreateTextFrame(w, h, "free", fontsize, "TOPLEFT", descriptionFrame, -2 * w, -4.9 * h, "Button")
    t:SetText("<<")
    f:EnableMouse(true)
    f:RegisterForClicks("LeftButtonUp")
    f:SetScript("OnClick", DeffEM.MoveToPreviousPage)


    DeffEM.UI.EventList = { }
    w = w * 4
    for i=0,7 do
        local frame = CreateFrame("Button", nil, mf, UIPanelButtonTemplate)
        frame:EnableMouse(true)
        DeffEM.InitButton(frame, w, h, path.."free", "MEDIUM")
        frame:SetPoint("BOTTOMLEFT", mf, 0, h * i)
        frame:SetID(i + 1)
        frame:RegisterForClicks("LeftButtonUp")
        frame:SetScript("OnClick", DeffEM.HandleEventInfoClick)
        DeffEM.CreateActiveTexture(frame)

        DeffEM.UI.EventList[i + 1] = { }
        local tbl = DeffEM.UI.EventList[i + 1]
        tbl.frame = frame
        f, t = DeffEM.CreateTextFrame(w / 3, h, "", fontsize, "LEFT", frame, 0)
        tbl.owner = t
        f, t = DeffEM.CreateTextFrame(w / 6, h, "", fontsize, "LEFT", frame, w / 3)
        tbl.id = t
        f, t = DeffEM.CreateTextFrame(w / 2, h, "", fontsize, "LEFT", frame, w / 2)
        tbl.name = t
    end

    DeffEM.ToggleEventSubInfo(DeffEM.LastActiveInfoButton)
    local counter = 0
    for id, event in orderedPairs(DeffEMSV.events) do
        if counter >= (DeffEMSV.EventPage - 1) * 8 then
            DeffEMSV.events["__orderedIndex"] = nil
            DeffEM.ShowEventList(id)
            break
        end
        counter = counter + 1
    end
end

