if not DeffEM then DeffEM = {} end
DeffEM.version = "2.2"

function DeffEM.GetPlvl()
    DeffEM.myPlvl = 0 -- @TODO: update me
end

function DeffEM.AtLogin()
    DeffEM.GetPlvl()
    DeffEM.SyncEvents()
    DeffEM.InitializeUI()
end

function DeffEM.Initialize()
    if not DeffEM.temp then DeffEM.temp = { } end
    if not DeffEMSV then DeffEMSV = { } end
    if not DeffEMSV.events then
        DeffEMSV.events = { }
    end
    DeffEMSV.version = "2.2"
    DeffEMSV.events["__orderedIndex"] = nil
    DeffEM.CalculateEventHash()
end

local freg = CreateFrame("Frame")
freg:RegisterEvent("ADDON_LOADED")
freg:SetScript("OnEvent", DeffEM.OnEvent)
local flog = CreateFrame("Frame")
flog:RegisterEvent("PLAYER_LOGIN")
flog:SetScript("OnEvent", DeffEM.AtLogin)
local fmsg = CreateFrame("Frame")
fmsg:RegisterEvent("CHAT_MSG_ADDON")
fmsg:SetScript("OnEvent", DeffEM.RcvMsg)
