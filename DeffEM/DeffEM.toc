## Interface: 30300
## Title: DeffEM
## Notes: DeffEM - AddOn pro Deffender EM / GM
## Version: 2.0
## Author: Xiledria
## SavedVariables: DeffEMSV
##Dependencies: Invajter
Core.lua
Event.lua
UIScripts.lua
Init.lua
Sha1.lua
SortedIterator.lua
UI.xml
